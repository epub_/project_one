# Отчетик о том как же все-таки работает наша программа.

Первая функция **_enter()_** проверяет строку цифр на соответствие всем значениям.

```
def enter(z):

  if not z.isdigit():
      raise TypeError()
  if len(z) % 2 != 0:
      raise ValueError()
  return z
```

![alt bbb](https://sun9-42.userapi.com/c858024/v858024079/134211/0T8rbI-zXZ4.jpg)

Основная функция **_decryption()_** выполняет саму програм очку.
Т.е:
* Принимает на вход проверенную строку цифр.
* Сравнивает 2 последовательные цифры с исходными в словаре
* Заменяет их на соответствующую букву или на пробел

Словарик для проверки пары цифр:

```
dictionary = {'00': 'A', '01': 'B', '02': 'C',
              '03': 'D', '04': 'E', '05': 'F',
              '10': 'G', '11': 'H', '12': 'I',
              '13': 'J', '14': 'K', '15': 'L',
              '20': 'M', '21': 'N', '22': 'O',
              '23': 'P', '24': 'Q', '25': 'R',
              '30': 'S', '31': 'T', '32': 'U',
              '33': 'V', '34': 'W', '35': 'X',
              '40': 'Y', '41': 'Z', '42': ' '}
```

Цикл для расшифровки строки:

```
for i in range(len(code) // 2):
        for key in dictionary.keys():
            if code[start:finish] == key:
                b.append(dictionary[key])
            elif code[start:finish] not in dictionary.keys():
                error = 'Ошибка ввода.'
                return error
                sys.exit()
```


![alt aaa](https://sun9-51.userapi.com/c858024/v858024079/134265/UuSZuZ-SZOs.jpg)

[Просто интересное видео](https://www.youtube.com/watch?v=yCzv3l143DM)