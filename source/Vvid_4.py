import sys
## Функция для проверки ввода.
#
# Принимает на вход последовательность цифр.
# Проверяет строку цифр на соответствие типу данных и 
# на четное количество цифр.
# 
# @param z - последовательность цифр.
# 
# @return Проверенную последовательность цифр.
# 
# @throw TypeError.
# @throw ValueError.
# 
#
# @b Examples:
# @code{.py}
# >>> enter(121212)
# 121212
# >>> enter(F)
# Traceback (most recent call last):
#     ...
#     raise TypeError()
# TypeError()
# 
# >>> enter(12122)
# Traceback (most recent call last):
#    ...
#    raise ValueError()
# ValueError()
# @endcode
#

def enter(z):

  if not z.isdigit():
      raise TypeError()
  if len(z) % 2 != 0:
      raise ValueError()
  return z

## Функция декодирования строки цифр.
#
# Принимает на вход проверенную строку цифр.
# Сравнивает 2 последовательные цифры с исходными в словаре
# и заменяет их на соответствующую букву или на пробел.
# 
# 
# @param z - проверенная строка цифр.
# 
# 
# @return Результат преобразований.
# 
#
# @bExamples:
# @code{.py}
#     >>> decryption(121212)
#     III
#     >>> decryption(06)
#     Ошибка ввода.
# @endcode
#
def decryption(code):
  
  
    b = []
    dictionary = {'00': 'A', '01': 'B', '02': 'C',
                  '03': 'D', '04': 'E', '05': 'F',
                  '10': 'G', '11': 'H', '12': 'I',
                  '13': 'J', '14': 'K', '15': 'L',
                  '20': 'M', '21': 'N', '22': 'O',
                  '23': 'P', '24': 'Q', '25': 'R',
                  '30': 'S', '31': 'T', '32': 'U',
                  '33': 'V', '34': 'W', '35': 'X',
                  '40': 'Y', '41': 'Z', '42': ' '}
    start = 0
    finish = 2
    for i in range(len(code) // 2):
        for key in dictionary.keys():
            if code[start:finish] == key:
                b.append(dictionary[key])
            elif code[start:finish] not in dictionary.keys():
                error = 'Ошибка ввода.'
                return error
                sys.exit()
        start = start + 2
        finish = finish + 2
    cd = ''.join(b)
    return cd
if __name__=='__main__':
    print(decryption(enter(input())))
