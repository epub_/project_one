from Vvid_4 import enter
import pytest

def test_alpha():
	"""Несоответствие типу данных"""
	with pytest.raises(TypeError):
 		enter("fgrt")

def test_parity():
	"""Неверное количество символов"""
	with pytest.raises(ValueError):
 		enter("12345")

def test_true():
	"""Случай без ошибок"""
	assert enter('1234') == '1234'