from Vvid_4 import decryption
import pytest

def test_true1():
	"""Случай без ошибок"""
	assert decryption('121200') == 'IIA'

def test_true2():
	"""случай без ошибок"""
	assert decryption('2300330415') == 'PAVEL'

def test_error1():
	"""Выход за границы значений"""
	assert decryption('46') == 'Ошибка ввода.'

def test_error2():
	"""Выход за границы значений"""
	assert decryption('7812') == 'Ошибка ввода.'

def test_error3():
	"""Выход за границы значений"""
	assert decryption('1299') == 'Ошибка ввода.'